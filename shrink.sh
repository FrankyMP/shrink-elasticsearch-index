#!/bin/bash

## get .env value as variable
export $(grep -v '^#' .env | xargs -d '\n')

now=$(date +"%Y-%m-%d")
beforeIndex=$(date --date="$retentionShrinkDay day ago" +"filebeat-7.10.2-%Y.%m.%d")
afterIndex=$(date --date="$retentionShrinkDay day ago" +"filebeat-7.10.2-%Y.%m.%d-shrinked")
retentionLimit=$(date --date="$retentionDay day ago" +"filebeat-7.10.2-%Y.%m.%d-shrinked")

{
echo "Shrink $now"

## Prepare Index for shrinking
curl -X PUT "$elasticUrl/$beforeIndex/_settings?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "index.number_of_replicas": 0,
    "index.routing.allocation.require._name": "$nodeName",
    "index.blocks.write": true
  }
}'

## Shrink Index 
curl -X POST "$elasticUrl/$beforeIndex/_shrink/$afterIndex" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "index.routing.allocation.require._name": null,
    "index.blocks.write": null,
    "index.number_of_replicas": 0,
    "index.number_of_shards": 1,
    "index.codec": "best_compression"
  }
}'

echo $'\n'
} >> shrink.log 2>/dev/null