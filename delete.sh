#!/bin/bash

## get .env value as variable
export $(grep -v '^#' .env | xargs -d '\n')

now=$(date +"%Y-%m-%d")
beforeIndex=$(date --date="$retentionShrinkDay day ago" +"filebeat-7.10.2-%Y.%m.%d")
retentionLimit=$(date --date="$retentionDay day ago" +"filebeat-7.10.2-%Y.%m.%d-shrinked")
todayIndex=$(date +"filebeat-7.10.2-%Y.%m.%d")

{
echo "Delete $now"

## Set index with 0 Replica to make Green Index in single node
curl -X PUT "$elasticUrl/$todayIndex/_settings" -H 'Content-Type: application/json' -d '
{
  "index" : {
    "number_of_replicas" : 0
  }
}
'

## Delete old Index after shrinking
curl -X DELETE "$elasticUrl/$beforeIndex" -H 'Content-Type: application/json'

## Delete old Index after xx Day
curl -X DELETE "$elasticUrl/$retentionLimit" -H 'Content-Type: application/json'

echo $'\n'
} >> shrink.log 2>/dev/null